Traefik Setup with SSL:
- Set your DOMAIN in ```.env``` variables file
- Modify your **docker.domain** and **acme.email** in **traefik.toml**
- ```touch acme.json && chmod 600 acme.json```
- ```docker-compose up -d```
- Note: We use different frontend rules for the **traefik** and **nginx_proxy** as this ensures both **www.** and **non-www** versions of the site are registered for SSL

Resources:
- https://blog.raveland.org/post/traefik_le/  
- [Redirection of www. to @](https://github.com/containous/traefik/issues/2796)